extends Node3D

const ROT_COOLDOWN = 0.12

@export var sun_altitude: Curve = null
@export var sun_energy: Curve = null
@export var sun_gradient: Gradient = null

@onready var time: WorldTime = $"../WorldTime"
@onready var env: WorldEnvironment = $"../WorldEnvironment"

var rot_cooldown = ROT_COOLDOWN

func _process(delta):
	var day_progress = time.day_progress
	
	if day_progress < 0.5:
		rot_cooldown -= delta
		
		if rot_cooldown < 0:
			rot_cooldown = ROT_COOLDOWN
		
			$DirectionalLight3D.rotation.x = -PI/4 * sun_altitude.sample(day_progress)
			rotation.y = lerpf(PI/2, -PI/2, 2 * day_progress)
	
	$DirectionalLight3D.visible = sun_energy.sample(day_progress) > 0.0001
	$DirectionalLight3D.light_color = sun_gradient.sample(day_progress)
	$DirectionalLight3D.light_energy = sun_energy.sample(day_progress)
	
