class_name Notifications
extends Control

const notif = preload("res://notifications/notification.tscn")


func _ready():
	add_quest_update()


func add_notification(text: String):
	var inst = notif.instantiate()
	
	$VBoxContainer.add_child(inst)
	$VBoxContainer.move_child(inst, 0)
	
	inst.set_text(text)


func add_quest_update():
	add_notification("Quest updated")
