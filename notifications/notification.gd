extends Panel

const LIVESPAN = 2.5
const FADE_TIME = 0.75

var time_left: float = LIVESPAN


func _physics_process(delta):
	time_left -= delta
	
	if time_left < 0:
		hide()
		queue_free()
	elif time_left < FADE_TIME:
		var color = Color.WHITE
		color.a = time_left / FADE_TIME
		modulate = color


func set_text(text: String):
	$Label.text = text
