class_name Root
extends Node3D

const INTERACTION_DIALOG = "dialog"
const INTERACTION_ACTION = "action"

const CTX_MAIN = 0
const CTX_SELECT = 1

@onready var dialog_mgr: DialogMgr = $DialogMgr

var input_context = 0

var block_interactions = false

var pointed_interaction = null
var area_interaction = null

var dialog_interaction = null


func _ready():
	get_tree().set_auto_accept_quit(false)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	$SelectMenu.visible = false
	$SelectMenu.rebuild_inv()


func _input(event):
	if event.is_action_released("start"):
		get_tree().root.propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST)
		get_tree().quit()
	elif event.is_action_released("select"):
		$SelectMenu.visible = not $SelectMenu.visible
		
		if $SelectMenu.visible:
			input_context = CTX_SELECT
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
			$SelectMenu/Map.grab_focus()
		else:
			input_context = CTX_MAIN
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
			get_viewport().set_input_as_handled()
	elif event.is_action_released("interact"):
		if $DialogMgr.visible:
			pass
		else:
			pass
	elif event is InputEventKey and event.is_released():
		match event.physical_keycode:
			Key.KEY_1:
				$TreeMgr.toggle_forest_mark()


func is_interactions_blocked() -> bool:
	return block_interactions


func interaction_enter(interaction):
	if block_interactions: return
	
	if pointed_interaction != null:
		interaction_exit(pointed_interaction)
	
	pointed_interaction = interaction
	re_int_state()


func interaction_exit(interaction):
	if block_interactions: return
	
	pointed_interaction = null
	re_int_state()


func interaction_area_enter(interaction):
	if block_interactions: return
	
	if area_interaction != null:
		interaction_area_exit(area_interaction)
	
	area_interaction = interaction
	re_int_state()


func interaction_area_exit(interaction):
	if block_interactions: return
	
	if area_interaction != null:
		area_interaction = null
		re_int_state()


func re_int_state():
	dialog_mgr.hide()
	$Interactions.hide()
	
	if pointed_interaction != null:		re_init_interaction(pointed_interaction)
	elif area_interaction != null:		re_init_interaction(area_interaction)


func re_init_interaction(inter):
	match inter.get_interaction_type():
		INTERACTION_ACTION:
			var labels = inter.get_interaction_names()
			var ids = inter.get_interaction_ids()
			$Interactions/DialogOptions.offer_new_entries(labels, ids)
			$Interactions.show()
		INTERACTION_DIALOG:
			dialog_interaction = inter
			dialog_mgr.show_dialog(inter.get_dialog_filename())


func _on_dialog_enter():
	if dialog_interaction != null and dialog_interaction.has_method("is_blocking_interaction"):
		block_interactions = dialog_interaction.is_blocking_interaction()
	else:
		block_interactions = false


func _on_dialog_over():
	dialog_interaction = true
	$InteractionCooldown.paused = false
	$InteractionCooldown.start(0.1)


func _on_interaction_cooldown_timeout():
	block_interactions = false
	pointed_interaction = null
	dialog_interaction = null
	re_int_state()


func _on_interactions_option_selected(id):
	if area_interaction != null:
		area_interaction.act(id)
