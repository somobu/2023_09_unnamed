# 2023_09_unnamed demo


## How to run

Grab [Godot 4.1](https://godotengine.org/download/archive/), import project and run.


## Attribution

**Plan de Paris vers 1300-1330 - ALPAGE** ([source](https://alpage.huma-num.fr/documents/ressources/cartes/CarteParis1300-1330.svg)) by Caroline Bourlet (IRHT), Nicolas Thomas (INRAP-LAMOP), Cédric Roms (INRAP) under [Creative Commons Attribution 2.0 France](https://creativecommons.org/licenses/by/2.0/fr/deed.en).


## License

Feel free to run and modify but please do not reuse or redistribute it.

