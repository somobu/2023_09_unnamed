class_name MyCamera
extends Node3D

@onready var root: Root = $"/root/Root"

@export_node_path("Node3D") var initial_target = null
@export var follow_dynamics: Dynamics3 = null

var target: Node3D = null

var gamepad_sens = Vector2(2.75, 2.0)
var mouse_sens = Vector2(0.3, 0.25)
var mouse_motion = Vector2.ZERO

var cam_dist: float = 0.9
var cam_offset_dist: float = 0.5

var currently_poiting: Object = null


@onready var cam_base: Node3D = $Base
@onready var cam_head: Node3D = $Base/Head
@onready var cam_offset: Node3D = $Base/Head/Offset
@onready var camera: Node3D = $Base/Head/Offset/Camera3D
@onready var clip_base_raycast: RayCast3D = $Base/Head/RayCast3D
@onready var clip_cam_raycast: RayCast3D = $Base/Head/Offset/ClipCamRaycast
@onready var pointer_raycast: RayCast3D = $Base/Head/Offset/Camera3D/PointerRaycast


func _ready():
	if follow_dynamics == null: follow_dynamics = Dynamics3.new()
	
	follow_dynamics.reset_states()
	follow_dynamics.calc_coeffs()
	follow_dynamics.set_value(global_position)
	
	if initial_target != null:
		target = get_node(initial_target)
		follow_dynamics.set_target(target.global_position)


func _input(event):
	if not root.input_context == Root.CTX_MAIN:
		return
	
	if event is InputEventMouseMotion:
		mouse_motion += event.relative * -1 * mouse_sens


func _process(delta):
	if target != null:
		global_position = target.global_position
#		global_position = follow_dynamics.step(delta, target.global_position)


func _physics_process(delta):
	
	# Camera rotation
	var valid_ctx = root.input_context == Root.CTX_MAIN
	if valid_ctx:
		var input_dir = Input.get_vector("look_right", "look_left", "look_down", "look_up") * gamepad_sens
		
		input_dir += mouse_motion
		mouse_motion = Vector2.ZERO
		
		cam_base.rotation.y += input_dir.x * delta
		cam_head.rotation.x = clampf(cam_head.rotation.x + input_dir.y * delta, -0.4*PI, 0.4*PI)
	
	
	# Camera clipping
	var offset_collision_point = clip_base_raycast.get_collision_point()
	var offset_collision_len = (clip_base_raycast.global_position - offset_collision_point).length() - 0.1
	cam_offset.position.x = minf(cam_offset_dist, offset_collision_len)
	
	var cam_collision_point = clip_cam_raycast.get_collision_point()
	var cam_collision_len = (clip_cam_raycast.global_position - cam_collision_point).length() - 0.75
	camera.position.z = minf(cam_dist, cam_collision_len)
	
	
	# Interactions / active objects
	var pointed_object = pointer_raycast.get_collider();
	
	if currently_poiting != pointed_object:
		
		if currently_poiting != null:
			if currently_poiting.has_method("on_pointer_exit"):
				currently_poiting.on_pointer_exit()
				
			if currently_poiting.has_method("get_interaction_type"):
				root.interaction_exit(currently_poiting)
		
		if pointed_object != null:
			if pointed_object.has_method("on_pointer_enter"):
				pointed_object.on_pointer_enter()
			
			if pointed_object.has_method("get_interaction_type"):
				root.interaction_enter(pointed_object)
		
		currently_poiting = pointed_object


func get_camera_basis_global() -> Basis:
	return camera.global_transform.basis


func get_camera_origin_global() -> Vector3:
	return camera.global_transform.origin


func get_cam_glob_rotation_y() -> float:
	return cam_base.global_rotation.y
