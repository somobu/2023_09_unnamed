@tool
class_name Dynamics3
extends Resource

@export var human_values: Vector3 = Vector3(1, 1, 0)


var k1 = 0.15
var k2 = 0.005
var k3 = 0.075


var target_value: Vector3 = Vector3.ZERO
var target_delta: Vector3 = Vector3.ZERO
var current_value: Vector3 = Vector3.ZERO
var current_delta: Vector3 = Vector3.ZERO


func calc_coeffs():
	var f = human_values.x
	var z = human_values.y
	var r = human_values.z
	
	k1 = z / (PI * f);
	k2 = 1 / ( (2 * PI * f) ** 2 )
	k3 = r * z / (2 * PI * f)


func reset_states():
	target_value = Vector3.ZERO
	target_delta = Vector3.ZERO
	current_value = Vector3.ZERO
	current_delta = Vector3.ZERO


func set_target(target: Vector3):
	target_value = target


func set_value(value: Vector3):
	current_value = value


func step(delta: float, new_target: Vector3) -> Vector3:
	# This velocity estimation is wrong, fixme!
	# Src: https://www.youtube.com/watch?v=KPoeNZZ6H4s
	target_delta = (new_target - target_value) / delta;
	
	current_value = current_value + current_delta * delta;
	current_delta = current_delta + delta * (target_value + k3 * target_delta - current_value - k1 * current_delta) / k2;
	
	target_value = new_target
	
	return current_value
