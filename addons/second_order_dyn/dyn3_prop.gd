@tool
class_name Dyn3Prop
extends EditorProperty

var control
var data: Vector3 = Vector3.ZERO
var shut_up = false

func _init():
	control = load("res://addons/second_order_dyn/dyn3_editor.tscn").instantiate()
	add_child(control)
	set_bottom_editor(control)
	add_focusable(control)
	
	var change_x = func(x):
		if shut_up: return
		data.x = x
		emit_changed(get_edited_property(), data)
		call_deferred("recalc_graph")
	control.get_node("Grid/Freq").connect("value_changed", change_x)
	
	var change_y = func(y):
		if shut_up: return
		data.y = y
		emit_changed(get_edited_property(), data)
		call_deferred("recalc_graph")
	control.get_node("Grid/Damp").connect("value_changed", change_y)
		
	var change_z = func(z):
		if shut_up: return
		data.z = z
		emit_changed(get_edited_property(), data)
		call_deferred("recalc_graph")
	control.get_node("Grid/Resp").connect("value_changed", change_z)


func _update_property():
	shut_up = true
	data = get_edited_object()[get_edited_property()]
	control.get_node("Grid/Freq").value = data.x
	control.get_node("Grid/Damp").value = data.y
	control.get_node("Grid/Resp").value = data.z
	call_deferred("recalc_graph")
	shut_up = false


func recalc_graph():
	var step_fcn: Array[Vector2] = []
	var resp_fcn: Array[Vector2] = []
	
	var dynamics: Dynamics3 = get_edited_object()
	dynamics.reset_states()
	dynamics.calc_coeffs()
	
	
	var rs = 100
	for i in range(0, 20*rs, 1):
		var step = 0.0
		if i > 1*rs: step = 1.0
		
		var resp = dynamics.step(1.0/rs, Vector3(step, 0, 0)).length()
		step_fcn.append(Vector2(float(i) / rs, step))
		resp_fcn.append(Vector2(float(i) / rs, resp))
	
	var graph: DynGraph = control.get_node("GraphPanel/Graph") 
	graph.update_graph(step_fcn, resp_fcn)
