@tool
class_name DynGraph
extends Control

var step_fcn: Array[Vector2] = []
var resp_fcn: Array[Vector2] = []

var total_graph_height = 2.0
var target_offset = Vector2(0, -total_graph_height + 0.5)

func update_graph(_step_fcn: Array[Vector2], _resp_fcn: Array[Vector2]):
	self.step_fcn = _step_fcn
	self.resp_fcn = _resp_fcn
	queue_redraw()


func _draw():
	var x_size = get_size().x
	var y_size = get_size().y
	var linear_scale: float = y_size / total_graph_height
	var scale = Vector2(linear_scale, -linear_scale)
	
	var grid_color = Color("#333333")
	for x in range(0, 10):
		var dx = x + target_offset.x
		draw_line(Vector2(dx*linear_scale, 0), Vector2(dx*linear_scale, y_size), grid_color)
	for y in range(-10, 10):
		var dy = y + target_offset.y
		draw_line(Vector2(0, dy*linear_scale), Vector2(x_size, dy*linear_scale), grid_color)
	
	for i in range(1, step_fcn.size()):
		var from: Vector2 = (step_fcn[i-1] + target_offset) * scale
		var to: Vector2 = (step_fcn[i] + target_offset) * scale
		draw_line(from, to, Color.GRAY, 1.0, false)
	
	for i in range(1, resp_fcn.size()):
		var from: Vector2 = (resp_fcn[i-1] + target_offset) * scale
		var to: Vector2 = (resp_fcn[i] + target_offset) * scale
		draw_line(from, to, Color.ORANGE, 0.75, true)
