@tool
extends EditorInspectorPlugin

const ignore_classes = [
	"TileSetAtlasSourceProxyObject",
	"TileSetScenesCollectionProxyObject",
	"SectionedInspectorFilter"
]

var dyn3prop = preload("res://addons/second_order_dyn/dyn3_prop.gd")

func _can_handle(object):
	return true

func _parse_property(object, type, name, hint_type, hint_string, usage_flags, wide):
	
	# Workaround
	if object.get_class() in ignore_classes: return
	
	var field = object[name]
	
	if object is Dynamics3:
		if name == "human_values":
			var i = Dyn3Prop.new()
			add_property_editor(name, i)
			return true
	
	return false
