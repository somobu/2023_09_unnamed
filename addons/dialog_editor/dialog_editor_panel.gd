@tool
extends Control

const char_ref = preload("res://addons/dialog_editor/char.tscn")
const node_ref = preload("res://addons/dialog_editor/node.tscn")

var plugin_ref: EditorPlugin

var files_list = []


@onready var chars = $HBoxContainer/Characters/VBoxContainer
@onready var graph: GraphEdit = $HBoxContainer/GraphEdit


func _ready():
	update_file_selector()
	
	if get_current_file() != null:
		load_selected_file()


func toggle_chars():
	$HBoxContainer/Characters.visible = not $HBoxContainer/Characters.visible


func update_file_selector():
	var current_file = get_current_file()
	
	var jsons = find_jsons("res://")
	
	$DialogsList.clear()
	for json in jsons:
		$DialogsList.add_item(json)
	
	var idx = jsons.find(current_file)
	if idx == -1 and jsons.size() > 0:
		$DialogsList.select(0)
	else:
		$DialogsList.select(idx)
	
	files_list = jsons
	
	if jsons.size() == 0:
		$DialogsList.hide()
		$BtnLoad.hide()
		$BtnSave.hide()
	else:
		$DialogsList.show()
		$BtnLoad.show()
		$BtnSave.show()


func find_jsons(path: String) -> Array:
	var jsons = []
	
	var da: DirAccess = DirAccess.open(path)
	for dir in da.get_directories():
		jsons.append_array(find_jsons(path + "/" + dir))
	
	for file in da.get_files():
		if (file as String).ends_with(".json"):
			jsons.append(path + "/" + file)
	
	return jsons


func on_dialogs_list_item_selected(index):
	load_selected_file()


func get_current_file():
	if files_list.size() <= 0 or $DialogsList.get_selected_id() >= files_list.size():
		return null
	else:
		return files_list[$DialogsList.get_selected_id()]


func load_selected_file():
	for child in chars.get_children():
		chars.remove_child(child)
		child.queue_free()
	
	graph.clear_connections()
	for child in graph.get_children():
		graph.remove_child(child)
		child.queue_free()
	
	assert(get_current_file() != null)
	
	var f = FileAccess.open(get_current_file(), FileAccess.READ)
	var dialog_data = JSON.parse_string(f.get_as_text())
	f.close()
	
	
	for char_id in dialog_data["chars"]:
		var state = dialog_data["chars"][char_id]
		var inst = char_ref.instantiate()
		inst.name = char_id
		inst.set_state(state)
		chars.add_child(inst)
	
	
	# Instantiate nodes and set state
	for entry_id in dialog_data["dialog"]:
		var entry = dialog_data["dialog"][entry_id]
		
		var node = node_ref.instantiate()
		node.name = entry_id
		node.connect("node_deletes", node_remove)
		node.set_state(entry)
		
		graph.add_child(node)
	
	# Setup reply -> node connections
	for entry_id in dialog_data["dialog"]:
		var entry = dialog_data["dialog"][entry_id]
	
		if "opts_forward" in entry:
			graph.connect_node(entry_id, 0, entry["opts_forward"], 0)
		else:
			for option_idx in range(entry["opts"].size()):
				var option = entry["opts"][option_idx]
				if "to" in option:
					graph.connect_node(entry_id, option_idx, option["to"], 0)


func save_selected_file():
	var result = {}
	
	# Save characters' profiles
	result["chars"] = {}
	for node in $HBoxContainer/Characters/VBoxContainer.get_children():
		var overriden_id = node.get_overriden_id()
		result["chars"][overriden_id] = node.get_state()
	
	
	# Save dialog nodes, replies and links
	result["dialog"] = {}
	for node in graph.get_children():
		var data = node.get_state()
		
		if node.is_forwading_opts:
			data["opts_forward"] = get_connections_from_all(node.name)[0]["to"]
		else:
			var options = data["opts"]
			for c in get_connections_from_all(node.name):
				options[c["from_port"]]["to"] = str(c["to"])
		
		result["dialog"][node.name] = data
	
	
	# Write this out
	assert(get_current_file() != null)
	
	var f = FileAccess.open(get_current_file(), FileAccess.WRITE)
	f.store_string(JSON.stringify(result, "\t"))
	f.flush()
	f.close()


func on_connection_request(from_node, from_port, to_node, to_port):
	for c in get_connections_from_port(from_node, from_port):
		graph.disconnect_node(c["from"], c["from_port"], c["to"], c["to_port"])
	
	graph.connect_node(from_node, from_port, to_node, to_port)


func on_connection_empty(from_node: String, from_port: int, pos: Vector2):
	var src_node = graph.get_node(str(from_node)) as GraphNode
	
	var node = node_ref.instantiate()
	node.name = from_node
	
	graph.add_child(node, true)
	node.position_offset = src_node.position_offset + pos + Vector2(220, -220)
	node.set_state({"char": src_node.char_id})
	
	node.reset_size()
	node.connect("node_deletes", node_remove)
	
	for c in get_connections_from_port(from_node, from_port):
		graph.disconnect_node(from_node, from_port, c["to"], c["to_port"])
	
	graph.connect_node(from_node, from_port, str(node.name), 0)


func on_disconnection_request(from_node, from_port, to_node, to_port):
	graph.disconnect_node(from_node, from_port, to_node, to_port)


func node_remove(node: String):
	print("Node remove: ", node)
	
	for c in get_connections_to_all(node):
		graph.disconnect_node(c["from"], c["from_port"], c["to"], c["to_port"])
		
	for c in get_connections_from_all(node):
		graph.disconnect_node(c["from"], c["from_port"], c["to"], c["to_port"])
	
	$HBoxContainer/GraphEdit.get_node(node).queue_free()


func reply_remove(node: String, port: int):
	
	# Удалим коннекшоны от реплая до нод
	for c in get_connections_from_port(node, port):
		graph.disconnect_node(node, port, c["to"], c["to_port"])
	
	# Сдвинем конекшоны после реплая на один вверх
	for c in get_connections_from_all(node):
		var src_port = c["from_port"]
		if src_port > port:
			graph.disconnect_node(node, src_port, c["to"], c["to_port"])
			graph.connect_node(node, src_port - 1, c["to"], c["to_port"])
	
	pass


func get_connections_from_all(node: String) -> Array:
	var result = []
	
	for entry in graph.get_connection_list():
		if entry["from"] == node :
			result.append(entry)
	
	return result


func get_connections_from_port(node: String, port: int) -> Array:
	var result = []
	
	for entry in graph.get_connection_list():
		if entry["from"] == node and entry["from_port"] == port:
			result.append(entry)
	
	return result


func get_connections_to_all(node: String) -> Array:
	var result = []
	
	for entry in graph.get_connection_list():
		if entry["to"] == node:
			result.append(entry)
	
	return result
