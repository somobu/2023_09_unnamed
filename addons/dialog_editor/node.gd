@tool
extends GraphNode

signal node_deletes(name: String)

const reply_ref = preload("res://addons/dialog_editor/reply.tscn")

var char_id = "dawg"
var text_mode = null

var is_forwading_opts = false


func set_state(entry: Dictionary):
	
	char_id = entry["char"]
	title = "%s | %s" % [ name, char_id ]
	
	if "text" in entry:
		set_message(entry["text"])
	
	if "opts_forward" in entry:
		is_forwading_opts = true
		set_slot_enabled_right(0, true)
		$HBoxContainer/BtnAdd.hide()
	
	elif "opts" in entry:
		for reply in entry["opts"]:
			add_reply(reply)
	
	if "text_mode" in entry:
		text_mode = entry["text_mode"]
	
	if "editor" in entry:
		var editor = entry["editor"]
		position_offset = Vector2(editor["px"], editor["py"])


func get_state() -> Dictionary:
	var data = {}
	
	data["char"] = char_id
	
	# Message
	var text = get_message() as String
	if not text.is_empty():
		data["text"] = text
	
	# Reply options
	if not is_forwading_opts:
		var options = []
		for i in range(1, get_child_count() - 1):
			options.append(get_reply(i).get_state())
		data["opts"] = options
	
	# Other data
	if text_mode != null:
		data["text_mode"] = text_mode
	
	# Editor-related things
	var editor = {
		"px": position_offset.x,
		"py": position_offset.y
	}
	data["editor"] = editor
	
	return data


func set_message(text = ""):
	$Control/Message.text = text


func get_message():
	return ($Control/Message.text as String).strip_edges()


func get_reply(idx: int):
	return get_child(idx)


func add_reply(state = null):
	var reply_node = reply_ref.instantiate()
	if state != null:
		reply_node.set_state(state)
	add_child(reply_node)
	move_child(reply_node, get_child_count() - 2)
	
	reply_node.connect("remove_requested", remove_reply)
	
	var new_count = get_connection_output_count()
	set_slot_enabled_right(get_child_count() - 2, true)


func remove_reply(reply_name: String):
	var child: Node = get_node(reply_name)
	# TODO: this sucks
	get_parent().get_parent().get_parent().reply_remove(name, child.get_index() - 1)
	child.queue_free()
	
	# Самый нижний слот у нас сча активен - отключим его
	var new_count = get_connection_output_count()
	set_slot_enabled_right(new_count, false)
	
	reset_size()


func _on_close_request():
	emit_signal("node_deletes", name)
