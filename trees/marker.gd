extends MeshInstance3D

const VISIBLE_DIST = 400.0
const DETAIL_DIST = 200.0

const L_NONE = 0
const L_LOW = 1
const L_HI = 2

var spruce_lo = preload("res://trees/spruce_lo.tscn")
var spruce_hi = preload("res://trees/spruce_hi.tscn")

@onready var player: Node3D = get_node("/root/Root/Player")
@onready var cooldown = (get_instance_id() % 1000) * 0.001

var current_detail = 0
var current_child: Node3D = null

func _physics_process(delta):
	
	cooldown -= delta
	if cooldown > 0: return
	cooldown = 1.0 + (get_instance_id() % 1000) * 0.001
	
	var dist = (player.global_position - global_position).length()
	
	var target_detail = L_NONE
	
	if dist < DETAIL_DIST:
		target_detail = L_HI
	elif dist < VISIBLE_DIST:
		target_detail = L_LOW
	else:
		target_detail = L_NONE
	
	if current_detail != target_detail:
		if current_child != null:
			current_child.queue_free()
		
		if target_detail == L_LOW:
			current_child = spruce_lo.instantiate()
			add_child(current_child)
		elif target_detail == L_HI:
			current_child = spruce_hi.instantiate()
			add_child(current_child)
		
		current_detail = target_detail
