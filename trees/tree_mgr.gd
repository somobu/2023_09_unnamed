extends Node3D

const CHUNKS_HALFSIDE = 10

const chunk = preload("res://trees/tree_sources.tscn")
const no_trees = preload("res://trees/tree_empty.tscn")

@onready var player: Node3D = get_node("/root/Root/Player")
@export var _chunkmap: Texture2D = null

var chunkmap: Image = null

func _ready():
	if _chunkmap != null:
		chunkmap = _chunkmap.get_image()


func _physics_process(delta):
	var pp = player.global_position
	var player_chunk = Vector2i(roundi(pp.x / Constants.CHUNK_SIDE), roundi(pp.z / Constants.CHUNK_SIDE) )
	
	if chunkmap != null:
		$Control/Panel/Label.text = str(chunkmap.get_size()/2 + player_chunk)
	
	var filled_positions: Array[Vector2i] = []
	var chunks_to_eliminate: Array[Node3D] = []
	
	for child in get_children():
		if child is Control: continue
		
		var cp: Vector3 = child.global_position
		var my_chunk = Vector2i(roundi(cp.x / Constants.CHUNK_SIDE), roundi(cp.z / Constants.CHUNK_SIDE) )
		
		if abs(player_chunk.x - my_chunk.x) > CHUNKS_HALFSIDE:
			chunks_to_eliminate.push_back(child)
		elif abs(player_chunk.y - my_chunk.y) > CHUNKS_HALFSIDE:
			chunks_to_eliminate.push_back(child)
		else:
			filled_positions.push_back(my_chunk)
	
	for child in chunks_to_eliminate:
		child.queue_free()
	
	for x in range (player_chunk.x - CHUNKS_HALFSIDE, player_chunk.x + CHUNKS_HALFSIDE + 1):
		for y in range (player_chunk.y - CHUNKS_HALFSIDE, player_chunk.y + CHUNKS_HALFSIDE + 1):
			var chpos = Vector2i(x,y)
			
			if not filled_positions.has(chpos):
				var inst: Node3D = null
				if can_spawn(chpos): inst = chunk.instantiate()
				else: inst = no_trees.instantiate()
				
				add_child(inst)
				inst.global_position = Vector3(chpos.x * Constants.CHUNK_SIDE, 0, chpos.y * Constants.CHUNK_SIDE)


func can_spawn(chunk: Vector2i) -> bool:
	if chunkmap == null: return true
	
	var img_chk = chunkmap.get_size()/2 + chunk
	var px = chunkmap.get_pixelv(img_chk)
	
	return px.g > 0.2



func toggle_forest_mark():
	var pp = player.global_position
	var player_chunk = Vector2i(roundi(pp.x / Constants.CHUNK_SIDE), roundi(pp.z / Constants.CHUNK_SIDE) )
	var img_chk = chunkmap.get_size()/2 + player_chunk
	
	var forest = can_spawn(player_chunk)
	chunkmap.set_pixelv(img_chk, Color(0.0, 0.0 if forest else 1.0, 0.0))
	
	write_image()
	reload_chunk(player_chunk)


func reload_chunk(chpos: Vector2i):
	for child in get_children():
		if child is Control: continue
		
		var cp: Vector3 = child.global_position
		var my_chunk = Vector2i(roundi(cp.x / Constants.CHUNK_SIDE), roundi(cp.z / Constants.CHUNK_SIDE) )
		
		if my_chunk == chpos:
			child.queue_free()
			break


func write_image():
	chunkmap.save_png("res://map/chunkmap_new.png")
