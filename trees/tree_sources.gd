extends Node3D

const SIMPLIFIED_DIST = 500.0

const single_trees = preload("res://trees/tree_sources_single_trees.tscn")

@onready var player: Node3D = get_node("/root/Root/Player")

var simplified_mode = true

func _physics_process(delta):
	
	var dist = (player.global_position - global_position).length()
	
	var simplified_mode_now = dist > SIMPLIFIED_DIST
	if simplified_mode != simplified_mode_now:
		simplified_mode = simplified_mode_now
		
		$GroupSprite.visible = simplified_mode
		
		if simplified_mode:
			var node = get_node_or_null("SingleTrees")
			if node != null:
				node.queue_free()
		else:
			var instance = single_trees.instantiate()
			instance.name = "SingleTrees"
			add_child(instance)
