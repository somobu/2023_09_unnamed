extends Node3D


@onready var root: Root = get_node("/root/Root")


func get_interaction_type() -> String:
	return Root.INTERACTION_DIALOG

func is_blocking_interaction() -> bool:
	return true

func get_dialog_filename() -> String:
	return "res://chars/dog/dialog_intro.json"



func on_pointer_enter():
	$Label3D.show()


func on_pointer_exit():
	$Label3D.hide()
