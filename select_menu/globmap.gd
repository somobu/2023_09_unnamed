extends Control

@onready var player_cam: MyCamera = $"/root/Root/Camera"
@onready var player = $"/root/Root/Player"

const map_scale: float = 1.0 / 4

func _process(delta):
	$Player.rotation = -player_cam.get_cam_glob_rotation_y()
	$Player.position = glob3_to_loc(player.global_position) - $Player.pivot_offset


func glob3_to_loc(glob: Vector3) -> Vector2:
	return Vector2(glob.x, glob.z) * map_scale
