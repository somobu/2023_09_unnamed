extends Control

@onready var time: WorldTime = $"/root/Root/WorldTime"
@onready var time_label: Label = $Time/Label

const WEEKDAYS: Array[String] = [
	"Mon",
	"Tue",
	"Wed",
	"Thu",
	"Fri",
	"Sat",
	"Sun"
]

const MONTHS: Array[String] = [
	"N/A",
	"Mid-winter",	# Jan
	"Gray Snow", 	# Feb
	"First Fain",	# Mar
	"Seed",			# Apr
	"[Month 05]", # May
	"Mid-summer",	# Jun
	"[Month 07]", # Jul
	"Harvest",		# Aug
	"[Month 09]", # Sep
	"Last Rain",	# Oct
	"Dark Trees",	# Nov
	"Frost",		# Dec
]

func _process(delta):
	var hd = time.get_human_date()
	var y = hd[0]
	var M = hd[1]
	var d = hd[2]
	
	var ht = time.get_human_time()
	var h = ht[0]
	var m = ht[1]
	
	var hw = time.get_human_week()
	var D = WEEKDAYS[hw[1]]
	var MM = MONTHS[M]
	
	time_label.text = "%s, %d of %s\n%d-%02d-%02d %02d:%02d" % [D, d, MM, y, M, d, h, m]


func _on_time_mouse_entered():
	$Time.grab_focus()


func _on_panel_mouse_entered():
	$Weather.grab_focus()
