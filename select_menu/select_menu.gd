class_name SelectMenu
extends Panel


func _on_chars_mouse_entered():
	$Chars.grab_focus()


func _on_inv_mouse_entered():
	$Inv.grab_focus()


func _on_quests_mouse_entered():
	$Quests.grab_focus()


func update_quest(title: String, text: String):
	$Quests/Title.text = title
	$Quests/Descr.text = text


func rebuild_inv():
	var inv: Inventory = get_node("/root/Root/Player/Inventory")
	$Inv/Label.text = "Инвентарь:\n" + inv.print_inv()
