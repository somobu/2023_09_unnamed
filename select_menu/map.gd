extends Button

@onready var root: Root = $"/root/Root"

var gamepad_map_move_sens = 200.0

var dragging = false

func _input(event):
	if not root.input_context == Root.CTX_SELECT:
		return
	
	if event is InputEventMouseButton:
		var is_pressed = event.pressed
		var is_lmb = event.button_index == MOUSE_BUTTON_LEFT
		
		if is_lmb and is_pressed and has_focus():
			dragging = true
		if is_lmb and not is_pressed:
			dragging = false
			
		if has_focus():
			if event.is_action("map_zoom_in"):
				$ScrollContainer/Globmap.scale *= 1.1
			elif event.is_action("map_zoom_out"):
				$ScrollContainer/Globmap.scale *= 0.9
		
	elif event is InputEventMouseMotion:
		if dragging:
			$ScrollContainer/Globmap.position += event.relative


func _process(delta):
	if not root.input_context == Root.CTX_SELECT:
		return
	
	if has_focus():
		var input_dir = Input.get_vector("look_left", "look_right", "look_up", "look_down")
		$ScrollContainer/Globmap.position -= input_dir * delta * gamepad_map_move_sens
	
	pass


func _on_mouse_entered():
	grab_focus()

