extends Area3D

const ELEV_SPEED = 3.0
var target_upper_floor = true


func get_interaction_type() -> String:
	return Root.INTERACTION_ACTION


func get_interaction_names() -> Array[String]:
	if target_upper_floor:
		return ["Ground floor"]
	else:
		return ["First floor"]

func get_interaction_ids() -> Array[String]:
	return [
		"ride"
	]


func act(idx: String):
	target_upper_floor = not target_upper_floor
	$CollisionShape3D.disabled = true


func _physics_process(delta):
	var elev: Node3D = get_parent()
	
	var step = ELEV_SPEED * delta
	var target_y = 4.1 if target_upper_floor else 0.0
	var curr_y = elev.position.y
	
	if abs(target_y - curr_y) > step:
		if curr_y < target_y:
			elev.position.y += step
		else:
			elev.position.y -= step
	else:
		if $CollisionShape3D.disabled:
			$CollisionShape3D.disabled = false
