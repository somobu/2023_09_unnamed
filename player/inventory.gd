class_name Inventory
extends Node


enum InvItemId {
	MONEY,
	DAWG_NOTE
}


class InvItemStack:
	var id: InvItemId = InvItemId.MONEY
	var count: int = 100


var items: Array[InvItemStack] = []

func _init():
	items.append(InvItemStack.new())


func print_inv() -> String:
	var rz = ""
	
	for item in items:
		rz += "    x" + ("%03d" % item.count) + " " + InvItemId.keys()[item.id] + "\n"
	
	return rz


func give_one(id: InvItemId):
	for item in items:
		if item.id == id:
			item.count += 1
			return
	
	var item = InvItemStack.new()
	item.id = id
	item.count = 1
	items.push_back(item)
