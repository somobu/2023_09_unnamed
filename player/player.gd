extends CharacterBody3D

const MOVE_SPEED = 8.0
const SPRINT_SPEED = 100.0 #20.0
const JUMP_VELOCITY = 5.5
const PLAYER_GRAVITY = 15.0

@onready var root: Root = $"/root/Root"
@onready var camera: MyCamera = $"../Camera"
@onready var anim: AnimationTree = $Model/AnimationTree

var cam_pivot_z = 0


func _physics_process(delta):
	
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= PLAYER_GRAVITY * delta
	
	var can_input = root.input_context == Root.CTX_MAIN and not root.is_interactions_blocked()
	
	
	# Handle Jump.
	if can_input and Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
	
	
	# Move and look
	
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
	var input_dir3 = Vector3(input_dir.x, 0, input_dir.y)
	
	var cam_basis = camera.get_camera_basis_global()
	var direction = cam_basis * input_dir3
	
	var is_sprint = Input.is_action_pressed("sprint")
	var speed = SPRINT_SPEED if is_sprint else MOVE_SPEED
	
	anim["parameters/conditions/walking"] = Vector2(velocity.x, velocity.z).length() > 0.001
	
	if can_input and direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
		$Model.rotation.y = camera.get_cam_glob_rotation_y() + PI
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)
		
	move_and_slide()
	
	
	# Adjust cam position by velocity
	var target_cpz = -abs((0.75 if is_sprint else 0.25) * velocity.length() / SPRINT_SPEED)
	cam_pivot_z += target_cpz - lerpf(cam_pivot_z, target_cpz, 1 - 5*delta)
	$Model/CameraTarget.position.z = cam_pivot_z
	
	
	# Hide character's model when cam is too close
	var dist_to_cam = ($Model/CameraTarget.global_position - camera.get_camera_origin_global()).length()
	$"Model/khajiit".visible = dist_to_cam > 0.5


func _on_action_area_entered(area: Area3D):
	if area.has_method("get_interaction_type"):
		root.interaction_area_enter(area)


func _on_action_area_exited(area: Area3D):
	if area.has_method("get_interaction_type"):
		root.interaction_area_exit(area)

