class_name DialogEntry
extends Control

signal text_shown

const visible_char_cooldown = 0.025

var current_vis_char_cooldown = 0

var grayed_out = false

func _ready():
	$TextLog/Label.text = "What took you so long?"


func _input(event):
	if not get_parent().visible: return
	if not visible: return
	
	if event.is_action_released("interact"):
		if not grayed_out and $TextLog/Label.visible_characters < $TextLog/Label.text.length() - 5:
			$TextLog/Label.visible_characters = $TextLog/Label.text.length()
			set_grayed_out(true)
			emit_signal("text_shown")


func _physics_process(delta):
	current_vis_char_cooldown -= delta
	
	if current_vis_char_cooldown < 0 and not grayed_out:
		current_vis_char_cooldown = visible_char_cooldown
		
		if $TextLog/Label.visible_characters < $TextLog/Label.text.length():
			$TextLog/Label.visible_characters += 1
		else:
			grayed_out = true
			emit_signal("text_shown")


func reset_line():
	$Person/Label.text = ""
	$TextLog/Label.text = ""


func set_dialog_line(author: String, message: String):
	$Person/Label.text = author
	$TextLog/Label.text = message
	$TextLog/Label.visible_characters = 1
	show()


func set_grayed_out(ga: bool):
	grayed_out = ga
	modulate = Color("#EEEEEE") if ga else Color.WHITE
