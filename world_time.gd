class_name WorldTime
extends Node

@export var days_count: int = 0
@export var day_progress: float = 0.025

func _process(delta):
	day_progress += (delta / Constants.DAY_LEN_SEC)
	
	if day_progress > 1.0: 
		day_progress -= 1.0
		days_count += 1


## Pair [hours, minutes]
func get_human_time() -> Array[int]:
	var h = floori(24 * day_progress)
	var m = floori(24 * 60 * day_progress - h * 60)
	
	var human_hours = (h + 9) % 24
	
	return [human_hours, m]


## Array [year, month, day]
## Months and days are counted from 1
func get_human_date() -> Array[int]:
	var cd = Constants.STARTING_DAY + days_count
	
	var y = floori(cd / Constants.DAYS_IN_YEAR)
	cd -= y * Constants.DAYS_IN_YEAR # leave only in-year part
	
	var m = floori(cd / Constants.DAYS_IN_MONTH)
	cd -= m * Constants.DAYS_IN_MONTH
	
	return [y, m + 1, cd + 1]


## Array [week no, week day]
## Counted from 0
func get_human_week() -> Array[int]:
	var cd = Constants.STARTING_DAY + days_count
	
	var weekday = cd % 7
	
	return [0, weekday]
