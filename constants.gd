class_name Constants
extends Node

## Side length, in units
const CHUNK_SIDE: float = 100.0

## In-game day length in real-world seconds
const DAY_LEN_SEC: float = 2000 * 60.0


# Calendar-related consts
const DAYS_IN_YEAR = 360
const DAYS_IN_MONTH = 30
const DAYS_IN_WEEK = 7

const STARTING_DAY = 1407 * DAYS_IN_YEAR + 1 * DAYS_IN_MONTH + 12
