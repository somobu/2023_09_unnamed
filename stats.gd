extends Node

const SAVE_COOLDOWN_SECS = 5 * 60.0
var save_cooldown = SAVE_COOLDOWN_SECS

var in_game_time_mins: int = 0
var in_game_time_sec: float = 0.0


func _ready():
	restore_state()


func _notification(what):
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		save_state()


func save_state():
	var data = {
		"igt": {
			"sec": in_game_time_sec,
			"min": in_game_time_mins
		}
	}
	
	var handle = FileAccess.open("user://stats.json", FileAccess.WRITE)
	handle.store_string(JSON.stringify(data))
	print("Stats saved")


func restore_state():
	var handle = FileAccess.open("user://stats.json", FileAccess.READ)
	if handle != null:
		var json = JSON.parse_string(handle.get_as_text())
		in_game_time_sec = json["igt"]["sec"]
		in_game_time_mins = json["igt"]["min"]


func _physics_process(delta):
	in_game_time_sec += delta
	if in_game_time_sec > 60.0:
		in_game_time_mins += 1
		in_game_time_sec -= 60.0
		print("In-game time: ", in_game_time_mins, " mins")
	
	save_cooldown -= delta
	if save_cooldown < 0:
		save_cooldown = SAVE_COOLDOWN_SECS
		save_state()
